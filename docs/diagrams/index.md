# Diagrams

For information on how to add or modify diagrams, see the [cheat sheet](cheatsheet.md).

## Pipeline dataflow

![Pipeline dataflow](pipeline-dataflow.png)
