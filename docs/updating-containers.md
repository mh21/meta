# Updating Containers

The CKI Project uses lots of different containers to run the pipeline itself
and the containers are also used in supporting projects.

## Repositories

Container source is stored in the [containers repository] and it consists of
a few pieces of code:

* Container build definitions (in Dockerfile format)
* Build script
* Additional files needed in each container

[containers repository]: https://gitlab.com/cki-project/containers

## Updating a container

When updating containers, please follow these guidelines:

* **Keep the containers minimal.** Only install the minimum amount of
  packages or other software that is needed for the container to do its job.

* **Clean up cached items.** Installing packages via package managers, like
  `dnf` and `pip`, leaves behind lots of cached files. Clean up packages with
  `dnf clean all` and remove the `pip` source cache with `rm -rf
  /root/.cache` before the container build is finished.

* **Update packages.** Ensure that each container build will download the
  latest packages available for the operating system release.

Start by cloning the container repository, creating a branch, and making
changes there. Push your branch to the container repository and monitor the
CI pipelines to ensure your container builds.

Open a merge request after the build completes and wait for a package review.

## Testing containers

Only tested containers should be marked with the date-based tags described
below. For merge requests, updated container images will be published to the
package registry of the fork with a `mr-1234` tag. Test this new version in the
pipeline to make sure nothing breaks. If the tests pass, proceed to the next
section.

## Releasing containers

Containers are built daily with the `latest` tag, but very few repositories
use the latest containers. Most use a tagged container, such as `20191031.1`.
This prevents unexpected drift in package versions or configurations in other
projects.

To release a *tested* container, create a tag with `git tag` or use GitLab's
interface by clicking *Repository*, then *Tags*, and then *New tag*. All tags
must be in the `YYMMDD.release` format.

For example the first release on October 31, 2019 would be: `20191031.1`. If a
new release is needed on the same day, increment the release number by one to
make `20191031.2`.

Once the CI pipelines are finished, the containers should be visible in
GitLab's container repository. Verify that by clicking *Packages* and then
*Container Registry*.

## Using newer containers in the pipeline definition

The [pipeline-definition] repository contains references to each container
that the pipeline uses. Make a new branch and change the `{image_tag}` variable
in the `cki_pipeline` and `gitlab-ci` YAML files. Note that internal build
containers use `internal_image_tag` variable instead, and this is the variable
you should be changing if you wish to switch the containers used for RHEL kernel
building.

Push your branch to GitLab and start a merge request. Add a comment to the
merge request that contains: `cki_bot test`. Wait for the pipelines to finish
and verify everything is correct before merging the changes!

[pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition
