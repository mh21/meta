# Creating new repositories

## Basic files

Start each repository with a `LICENSE` file
([GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) by default) and a
`README.md` file. Use the following template for the latter:

```markdown
# [Project name]

Summary of what this project is, and why it's needed.

## Maintainers

[main maintainer] (primary) and [secondary maintainer] (secondary)

## Design principles

Write here the main design decisions behind this project, so that a
person wanting to get familiar with this code will understand the
principles.

## Developer guidelines

Write here what to consider and take into account before merging
changes into this repo, or tips while developing.

## Development

If relevant, explain here how to get a development version of the
project up and running. Also add information on how to run tests,
coverage checkers, linters, etc.

## Tests before merging changes

Write here the manual or semi-automated tests that should be performed
before merging any changes to this repo, along with instructions to
easily reproduce the results.
```

The [containers repository] is a good example of how to complete the README
template.

[containers repository]: https://gitlab.com/cki-project/containers/blob/main/README.md

## Pull/Merge Request template

This is a useful template for the default text in Pull/Merge
Requests. Feel free to tweak it to suit your project better,
especially the first paragraph.

```markdown
# For the requester

Make sure you have tested this properly before submitting your
request, e.g. by following the "Tests before merging changes" section
in the project `README.md`.

Also, please review our
[development guidelines](https://docs.cki-project.org/coding.html#i-want-to-submit-a-change)
from time to time and make sure you stay familiar with them.

# For the reviewer

Please go through the [reviewer short checklist](https://docs.cki-project.org/coding.html#i-want-to-review-a-change) before merging.
```

To set this text as default PR/MR text, see the
[GitHub documentation](https://docs.github.com/en/github/building-a-strong-community/creating-a-pull-request-template-for-your-repository)
or the
[GitLab documentation](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-merge-requests-and-issues),
depending on where your project is hosted.
