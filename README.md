# CKI Project Documentation

This repository contains the source for the
[public documentation website] for the CKI Project.

## Making quick edits

- Browse the [docs/](docs) directory and click on `Edit`/`Web IDE` at the top of a file
- Click on the `Edit on GitLab` link in the top right hand corner of a
  page on the [public documentation website]

## Making longer edits

- Run `mkdocs serve`
- Load [http://localhost:8000] in a browser
- Edit whichever files you want, see the changes happen instantly on
  the browser
- Lint the files with `mdl .`
- When you are done, file a regular merge request with your changes

[http://localhost:8000]: http://localhost:8000
[public documentation website]: http://docs.cki-project.org/
